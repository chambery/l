library taffy;

//import 'package:logging_handlers/logging_handlers_shared.dart';

class Taffy {
//  final _log = new Logger("taffy");

  String name;
  Map db = {};
  List all = [];

  Taffy(this.name, List list) {
    all = new List.from(list);
    for(Map obj in list) {
        for(var key in obj.keys) {
	        /* map -> map -> map -> list */
//	     	print("${key} : ${obj[key]}");
	        db.putIfAbsent(key, () => {});

	        db[key].putIfAbsent("${obj[key]}", () => []);
//	        print("?[${key}][${obj[key]}] : ${db[key]["${obj[key]}"]}");
	        db[key]["${obj[key]}"].add(obj);
//			print(">[${key}][${obj[key]}] : ${db[key]["${obj[key]}"]}");
	    }
    }
//     for(var key in db.keys) {
  //   	print("${key} : ${db[key]}");
    // }
  }

  /**
   * Find first instance
   */
  Map first(String key, var value) {
  	var list = find(key, value);
  	if(list.isEmpty) {
  		return {};
  	}
    print("${list[0]}");
  	return list[0];
  }

  List find(String key, var value, [Map conditions]) {
  	var list = [];
    if(db.containsKey(key) && db[key].containsKey("${value}"))
    {
      list = db[key]["${value}"];
    	if(conditions != null)
      {
        var tmp = new List.from(list);
	      for(var key in conditions.keys)
        {
  	   		for(var obj in list)
          {
            print("conditions: ${key}:${conditions[key]} -> ${obj}");
		    		if(obj[key] != conditions[key])
            {
		    			tmp.remove(obj);
		    		}
		    	}
		    }
        list = tmp;
    	}
    	else
    	{
    		list = db[key]["${value}"];
    	}

      for(var item in list)
      {
        print("${item}");
      }
    }

  	return list;
  }

}