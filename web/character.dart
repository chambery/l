library character;
import "dart:html";
import "dart:json";
import "dart:async";
import "dart:mirrors";

class Character {
	final String PREFIX = "Character -- ";
	Map<String, int> abilities = {};
	/* name > level, spells[] */
	Map<String, Map> classes = {};
	List languages = [];
	String name;
	int xp;
	int hp;
	var log = [];
	var options = {};
	String race_name;
	String alignment;
	String goodness;
	String deity;
	String bloodline;
	List skills = [];
	List feats = [];

	Character.fromName(String owner, String name) {
		print("${PREFIX} Character.fromName(${owner}, ${name})");
		HttpRequest request = new HttpRequest();
		request.open("GET", "../web/out/user/${owner}/${name}/${name}", async : false);
  	request.send();

		print("${PREFIX} HttpRequest.open(\"user/${owner}/${name}\") => ${request.statusText} : ${request.responseText}");
			var map = parse(request.responseText);
      print("${PREFIX} chardata: ${map}");
      if(map["abilities"] != null) {
				abilities = map["abilities"];
	      print("${PREFIX} abilities: ${abilities}");
      }
/*      if(map["classes"] != null) {
				classes = map["classes"];
      }
      if(map["log"] != null) {
				log = map["log"];`
      }
      if(map["languages"] != null) {
      	languages = map["languages"];
      }
      */

	}

	String toString()
	{
		InstanceMirror im = reflect(this);
    ClassMirror cm = im.type;
    var string = "";
		for (var m in cm.getters.values) {
			string += im.invoke(m.simpleName, null);
			print("\t${PREFIX} toString: ${string}");
		}
		print("${PREFIX} toString: ${string}");
	return string;
	}
}
