import 'package:web_ui/web_ui.dart';

class ToggleComponent extends WebComponent {
	var name;
	var state = false;
  var collapse_img = "images/expanded.png";
  
 
  void toggle() {
    state = !state;
    print(query("#toggle_${name}"));
	query("#toggle_${name}").hidden = state;
	collapse_img = state ? "images/collapsed.png" : "images/expanded.png";
    
  }


}
