library loader;

import "dart:html";
import "dart:json";
import "dao.dart";
import "dart:async";

class Loader {
	Future load(String file, DAO dao) {
		HttpRequest.getString(file).then((response) {
			var list = parse(response);
      print("${list}");
      //RegExp filename = new RegExp("^.*/(.*?)\..*?$");
      var filename = file.substring(file.lastIndexOf("/") + 1, file.lastIndexOf("."));
      print("${filename}");
      dao.add(filename, list);
			return new Future.value(list);
		});
	}
}
